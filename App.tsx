import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './src/screens/HomeScreen/HomeScreens';
import StepOneScreen from "./src/screens/StepOneScreen/StepOneScreen";
import StepTwoScreen from "./src/screens/StepTwoScreen/StepTwoScreen";
import MenuAppScreen from "./src/screens/MenuAppScreen/MenuAppScreen";

export type RootStackParamList = {
    Home: undefined;
    StepOne: undefined;
    StepTwo: { steptwo: string };
    Form: undefined;
    FormScreen: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
                <Stack.Screen name="StepOne" component={StepOneScreen} options={{ headerShown: false }} />
                <Stack.Screen name="StepTwo" component={StepTwoScreen} options={{ headerShown: false }}/>
                <Stack.Screen name="Form" component={MenuAppScreen}  options={{ headerShown: false }}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;
