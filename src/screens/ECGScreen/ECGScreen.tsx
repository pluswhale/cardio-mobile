import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import Camera from "../../../assets/camera.png"

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

function EcgScreen() {
    return (
        <View style={styles.container}>
            <Image
                width={200}
                height={200}
                source={Camera}
            />
        </View>
    );
}

export default EcgScreen;
