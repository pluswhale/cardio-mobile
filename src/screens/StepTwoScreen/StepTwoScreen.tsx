import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, TextInput, RadioButton, Text, Headline } from 'react-native-paper';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RouteProp } from '@react-navigation/native';
import { RootStackParamList } from '../../../App';

type StepTwoScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'StepTwo'>;
type StepTwoScreenRouteProp = RouteProp<RootStackParamList, 'StepTwo'>;

type Props = {
    navigation: StepTwoScreenNavigationProp;
    route: StepTwoScreenRouteProp;
};

function StepTwoScreen({ navigation, route }: Props) {
    const { steptwo: option } = route.params;
    const [number, setNumber] = useState('');
    const [hospital, setHospital] = useState('');

    const hospitalOptions = ['Больница №1 отд. кардиологии', 'Больница №2 отд. кардиологии'];

    return (
        <View style={styles.container}>
            <Headline>Кардио Гуру</Headline>
            {option === 'emergency' ? (
                <View>
                    <Text>Номер бригады скорой помощи:</Text>
                    <TextInput
                        label="Введите текст"
                        value={number}
                        onChangeText={setNumber}
                    />
                    <View style={styles.buttonContainer}>
                        <Button onPress={() => navigation.goBack()}>Назад</Button>
                        <Button disabled={number === ''} onPress={() => navigation.navigate('Form')}>Ок</Button>
                    </View>
                </View>
            ) : (
                <View>
                    <Text>Выберети учеждение здравоохранения:</Text>
                    <RadioButton.Group onValueChange={setHospital} value={hospital}>
                        {hospitalOptions.map((value) => (
                            <RadioButton.Item key={value} label={value} value={value} />
                        ))}
                    </RadioButton.Group>
                    <View style={styles.buttonContainer}>
                        <Button onPress={() => navigation.goBack()}>Назад</Button>
                        <Button disabled={hospital === ''} onPress={() => navigation.navigate('Form')}>Ок</Button>
                    </View>
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 16,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 16,
    },
});
export default StepTwoScreen;