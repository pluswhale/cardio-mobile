import React from 'react';
import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs';
import {View, StyleSheet, Image} from 'react-native';
import EcgScreen from "../ECGScreen/ECGScreen";
import {NavigationContainer, NavigationProp} from "@react-navigation/native";
import {RootStackParamList} from "../../../App";
import CardPacientScreen from "../CardPacientScreen/CardPacientScreen";
import CardPacient from '../../../assets/file-text.png';
import ecg from '../../../assets/sunrise.png'
import HomeScreen from "../HomeScreen/HomeScreens";
import StepOneScreen from "../StepOneScreen/StepOneScreen";
import StepTwoScreen from "../StepTwoScreen/StepTwoScreen";
import FormScreen from "../FormScreen/FormScreen";
import {createStackNavigator} from "@react-navigation/stack";

type MenuNavigationProp = NavigationProp<RootStackParamList, 'Form'>;
const Stack = createStackNavigator<RootStackParamList>();



type Props = {
    navigation: MenuNavigationProp;
};

const Tab = createBottomTabNavigator();

const CustomTabBar = (props: any) => (
    <View style={styles.tabBar}>
        <BottomTabBar {...props} />
    </View>
);

const styles = StyleSheet.create({
    tabBar: {
        borderRadius: 20,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
    },
});

const MenuAppScreen = ({ navigation }:Props) => {
    return (
        <Stack.Navigator initialRouteName="FormScreen" >
            <Stack.Screen name="Home" options={{ headerShown: false }}>
                {() => (
                    <Tab.Navigator tabBar={props => <CustomTabBar {...props} />}>
                        <Tab.Screen
                            name="Снятие ЭКГ"
                            component={EcgScreen}
                            options={{
                                headerShown: false, // Add this line
                                tabBarIcon: ({ color, size }) => (
                                    <Image
                                        width={size}
                                        height={size}
                                        source={ecg}
                                    />
                                ),
                            }}
                        />
                        <Tab.Screen
                            name="Карта пациента"
                            component={CardPacientScreen}
                            options={{
                                headerShown: false, // Add this line
                                tabBarIcon: ({ color, size }) => (
                                    <Image
                                        width={size}
                                        height={size}
                                        source={CardPacient}
                                    />
                                ),
                            }}
                        />
                        <Tab.Screen
                            name="FormScreen"
                            component={FormScreen}
                            options={{
                                headerShown: false,
                                tabBarButton: () => null
                            }}
                        />
                    </Tab.Navigator>
                )}
            </Stack.Screen>
        </Stack.Navigator>
    );
};


export default MenuAppScreen;
