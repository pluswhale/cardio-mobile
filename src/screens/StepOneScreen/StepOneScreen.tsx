import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Text, Title } from 'react-native-paper';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RootStackParamList } from '../../../App';


type StepOneScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'StepOne'>;

type Props = {
    navigation: StepOneScreenNavigationProp;
};

function StepOneScreen({ navigation }: Props) {
    return (
        <View style={styles.container}>
            <Title style={styles.title}>Кардио Гуру</Title>
            <View style={styles.buttonContainer}>
                <Button
                    mode="contained"
                    onPress={() => navigation.navigate('StepTwo', { steptwo: 'emergency' } ) }
                    style={styles.button}
                >
                    Бригады скорой мед. помощи
                </Button>
                <Button
                    mode="contained"
                    onPress={() => navigation.navigate('StepTwo', { steptwo: 'hospital' })}
                    style={styles.button}
                >
                    Учреждение здравоохранения
                </Button>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 16,
    },
    title: {
        fontSize: 24,
        marginBottom: 30,
        textAlign: 'center',
    },
    buttonContainer: {
        width: '100%',
    },
    button: {
        marginBottom: 20,
    },
});

export default StepOneScreen;
