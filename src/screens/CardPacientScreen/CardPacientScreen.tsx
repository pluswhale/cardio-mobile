import React from 'react';
import { Button } from 'react-native-paper';
import {View, Text, StyleSheet, Image} from 'react-native';
import ECG from '../../../assets/image 29.png'

export default function CardPacientScreen({ navigation }:any) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Кардиограмма</Text>
            <Image source={ECG}/>
            <Text style={styles.date}>04.02.2024 13:46</Text>
            <Text style={styles.patientInfo}>Patient Name and Surname</Text>
            <View style={styles.buttonContainer}>
                <Button mode="contained" onPress={() => {}}>
                    <Text style={{ fontSize: 10 }}>отправить без данных</Text>
                </Button>

                <Button mode="contained" onPress={() => navigation.navigate('FormScreen')}>
                    <Text style={{ fontSize: 10 }}>заполнить данные</Text>
                </Button>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        height: '40%',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        marginTop: 50,
        marginHorizontal: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    image: {
        width: '100%',
        height: '100%',

        resizeMode: 'contain',
    },
    date: {
        fontSize: 18,
        color: '#888',
    },
    patientInfo: {
        fontSize: 20,
        textAlign: 'center',
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
    },
});
