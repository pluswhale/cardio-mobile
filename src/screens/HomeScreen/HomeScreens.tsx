import React, { useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import { NavigationProp } from '@react-navigation/native';
import { View, Text, TextInput, Button, StyleSheet } from 'react-native';
import {RootStackParamList} from "../../../App";


type HomeScreenNavigationProp = NavigationProp<RootStackParamList, 'Home'>;

type Props = {
    navigation: HomeScreenNavigationProp;
};

function HomeScreen({ navigation }: Props) {

    const [state, setState] = useState({
        email: '',
        password: '',
    });

    function handleChange(name: string, value: string) {
        setState(prevState => ({ ...prevState, [name]: value }));
    }
    async function handleSubmit() {
        const res = await fetch(`http://192.168.8.85:8001/api/login`, {
            method: 'POST',
            body: JSON.stringify(state),
            headers: {
                'Content-Type': 'application/json',
            },
        });

        if (res.ok) {
            const json = await res.json();
            await AsyncStorage.setItem('token', json.access_token);

            const profileRes = await fetch('http://192.168.8.85:8001/api/profile', {
                headers: {
                    Authorization: `Bearer ${json.access_token}`,
                },
            });

            // If the profile request is successful, navigate to StepOneScreen
            if (profileRes.ok) {
                navigation.navigate('StepOne');
            }
        }
    }


    return (
        <View style={styles.container}>
            <View style={styles.box}>
                <Text style={styles.title}>
                    <Text style={styles.heading}>Кардио Гуру</Text>
                    {/*<SvgUri width="20" height="20" source={require('../public/heart.svg')} />*/}
                </Text>
                <View style={styles.form}>
                    <Text style={styles.label}>Логин</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={value => handleChange('email', value)}
                        value={state.email}
                        placeholder="email"
                        autoComplete="off"
                    />
                    <Text style={styles.label}>Пароль</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={value => handleChange('password', value)}
                        value={state.password}
                        placeholder="password"
                        secureTextEntry
                    />
                    <Button title="ВОЙТИ" onPress={handleSubmit} />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    box: {
        backgroundColor: '#F4F5F7',
        width: '80%',
        padding: 25,
        borderRadius: 10,
    },
    title: {
        fontSize: 19,
        paddingBottom: 30,
        textAlign: 'center',
    },
    heading: {
        marginRight: 10,
    },
    form: {
        alignItems: 'center',
    },
    label: {
        paddingBottom: 20,
    },
    input: {
        height: 40,
        width: '100%',
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 50,
    },
});

export default HomeScreen;
