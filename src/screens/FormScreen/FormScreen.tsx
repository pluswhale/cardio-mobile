import React, { useState } from 'react';
import {ScrollView, View, StyleSheet, ViewStyle, TextStyle} from 'react-native';
import {RadioButton, Text, TextInput, Checkbox, Button, TouchableRipple} from 'react-native-paper';

const FormScreen = () => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [patronymic, setPatronymic] = useState('');
    const [address, setAddress] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [sex, setSex] = useState('M');
    const [confirmation, setConfirmation] = useState();
    const [cardioFile, setCardioFile] = useState('');
    const [height, setHeight] = useState('');
    const [weight, setWeight] = useState('');
    const [diabetes, setDiabetes] = useState('Нет');

    const [pacemaker, setPacemaker] = useState('Нет');
    const [takingMedications, setTakingMedications] = useState();
    const [medications, setMedications] = useState('');
    const [notes, setNotes] = useState('');
    const [ambulanceCrewNumber, setAmbulanceCrewNumber] = useState('');
    const [emergencyFlag, setEmergencyFlag] = useState();
    const [userId, setUserId] = useState();

    //radioButtons

    const [takingMedicines, setTakingMedicines] = useState('');
    const [isDiagnosis, setIsDiagnosis] = useState('');


    const handleLastNameChange = (text: string) => setLastName(text);
    const handleFirstNameChange = (text: string) => setFirstName(text);
    const handlePatronymicChange = (text: string) => setPatronymic(text);
    const handleAddressChange = (text: string) => setAddress(text);
    const handleBirthDateChange = (text: string) => setBirthDate(text);
    const handleHeightChange = (text: string) => setHeight(text);
    const handleWeightChange = (text: string) => setWeight(text);
    const handleDiagnosisChange = (text: string) => setIsDiagnosis(text);
    const handleMedicinesChange = (text: string) => setMedications(text);
    const handleNoteChange = (text: string) => setNotes(text);


    const sexOptions = ['М', 'Ж'];
    const diabetesOptions = ['Да', 'Нет'];
    const pacemakerOptions = ['Да', 'Нет'];
    const takingMedicinesOptions = ['Да', 'Нет'];
    const diagnosisOptions = ['Да', 'Нет'];
    const alcoholOptions = ['Да', 'Нет'];

    const handleSubmit = async () => {
        const patientData = {
            firstName,
            lastName,
            patronymic,
            address,
            birthDate,
            height,
            weight,
            sex,
            confirmation,
            diabetes,
            pacemaker,
            takingMedications,
            medications,
            notes,
            ambulanceCrewNumber,
            emergencyFlag
            // userId удален
        };

        const response = await fetch('http://192.168.8.201:8001/api/cardios/create', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(patientData)
        });

        try {
            const data = await response.json();
            console.log(data);
        } catch (e) {
            console.log(e)
        }


    };




    const [checked , setChecked] = React.useState(false)

    return (
        <ScrollView contentContainerStyle={{ paddingBottom: 70  , backgroundColor: 'transparent' }}>
        <View style={styles.container}>
            <Text style={{fontSize: 24, fontWeight: 'bold'}}>Заполенние данных о пациенте</Text>

            <Text style={{fontSize: 20, color: 'teal'}}>Общее</Text>

            <TextInput label='Фамилия' value={lastName} onChangeText={handleLastNameChange}/>
            <TextInput label='Имя' value={firstName} onChangeText={handleFirstNameChange}/>
            <TextInput label='Отчество' value={patronymic} onChangeText={handlePatronymicChange}/>


                <RadioButton.Group onValueChange={newValue => setSex(newValue)} value={sex}>
                    <View style={styles.wrapper_radio}>
                        <Text style={styles.label}>Пол</Text>
                        {sexOptions.map((option) => (
                            <TouchableRipple key={option} onPress={() => setSex(option)} style={styles.option(sex === option)}>
                                <Text style={styles.optionLabel}>{option}</Text>
                            </TouchableRipple>
                        ))}
                    </View>
                </RadioButton.Group>

            <TextInput label='Дата рождения' value={birthDate} onChangeText={handleBirthDateChange}/>
            <TextInput label='Адрес прописки' value={address} onChangeText={handleAddressChange}/>

                <Checkbox.Item
                    label="Информация подтверждена"
                    status={checked ? 'checked' : 'unchecked'}
                    onPress={() => { setChecked(!checked); }}
                />

            <Text style={{fontSize: 20, color: 'teal'}}>Данные о пациенте</Text>

                <RadioButton.Group onValueChange={newValue => setDiabetes(newValue)} value={diabetes}>
                    <View style={styles.wrapper_radio}>
                        <Text style={styles.label}>Сахарный диабет*</Text>
                        {diabetesOptions.map((option) => (
                            <TouchableRipple key={option} onPress={() => setDiabetes(option)} style={styles.option(diabetes === option)}>
                                <Text style={styles.optionLabel}>{option}</Text>
                            </TouchableRipple>
                        ))}
                    </View>
                </RadioButton.Group>
                <RadioButton.Group onValueChange={newValue => setPacemaker(newValue)} value={pacemaker}>
                    <View style={styles.wrapper_radio}>
                        <Text style={styles.label}>Кардиостимулятор*</Text>
                        {pacemakerOptions.map((option) => (
                            <TouchableRipple key={option} onPress={() => setPacemaker(option)} style={styles.option(pacemaker === option)}>
                                <Text style={styles.optionLabel}>{option}</Text>
                            </TouchableRipple>
                        ))}
                    </View>
                </RadioButton.Group>

                <RadioButton.Group onValueChange={newValue => setTakingMedicines(newValue)} value={takingMedicines}>
                    <View style={styles.wrapper_radio}>
                        <Text style={styles.label}>Приём лекарственных препаратов перед снятием ЭКГ</Text>
                        {takingMedicinesOptions.map((option) => (
                            <TouchableRipple key={option} onPress={() => setTakingMedicines(option)} style={styles.option(takingMedicines === option)}>
                                <Text style={styles.optionLabel}>{option}</Text>
                            </TouchableRipple>
                        ))}
                    </View>
                </RadioButton.Group>

                <Text style={{fontSize: 20, color: 'teal'}}>Какие лекарства были приняты</Text>

                <RadioButton.Group onValueChange={newValue => setIsDiagnosis(newValue)} value={isDiagnosis}>
                    <View style={styles.wrapper_radio}>
                        <Text style={styles.label}>Есть ли сопутствующие диагнозы</Text>
                        {diagnosisOptions.map((option) => (
                            <TouchableRipple key={option} onPress={() => setIsDiagnosis(option)} style={styles.option(isDiagnosis === option)}>
                                <Text style={styles.optionLabel}>{option}</Text>
                            </TouchableRipple>
                        ))}
                    </View>
                </RadioButton.Group>

                <RadioButton.Group onValueChange={newValue => setAmbulanceCrewNumber(newValue)} value={ambulanceCrewNumber}>
                    <View style={styles.wrapper_radio}>
                        <Text style={styles.label}>Курение</Text>
                        {alcoholOptions.map((option) => (
                            <TouchableRipple key={option} onPress={() => setAmbulanceCrewNumber(option)} style={styles.option(ambulanceCrewNumber === option)}>
                                <Text style={styles.optionLabel}>{option}</Text>
                            </TouchableRipple>
                        ))}
                    </View>
                </RadioButton.Group>

                <TextInput label='Рост' value={height} onChangeText={handleHeightChange}/>
                <TextInput label='Вес' value={weight} onChangeText={handleWeightChange}/>

                <Text style={{fontSize: 20, color: 'teal'}}>Примечание</Text>

                <TextInput label='введите текст' value={notes} onChangeText={handleNoteChange}/>
            <Button
                mode="contained"
                onPress={handleSubmit}
                style={{marginTop: 10, backgroundColor: '#956eff'}}
            >
                Отправить
            </Button>
        </View>
        </ScrollView>

    );
};

const styles = {
    container: {
        padding: 16,
        paddingTop: 50
    } as ViewStyle,
    wrapper_radio: {
        flexDirection: 'row' as 'row',
        alignItems: 'center' as 'center',
        padding: 10,
        flexWrap: "wrap",
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    } as ViewStyle,
    label: {
        fontWeight: 'bold' as 'bold',
        color: '#000',
        marginRight: 20,
    } as TextStyle,
    option: (selected: boolean) => ({
        flexDirection: 'row' as 'row',
        alignItems: 'center' as 'center',
        marginLeft: 10,
        backgroundColor: selected ? '#fff' : '#eee',
    }) as ViewStyle,
    optionLabel: {
        padding: 10 ,
        paddingLeft: 20,
        paddingRight: 20 ,
        alignItems: 'center',
        justifyContent: 'center'
    } as TextStyle,
};

export default FormScreen;